var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var profileSchema = new Schema(
    {
        nama: {type: String, require: true},
        nim: {type: Number, require: true}
    }
);

module.exports = new mongoose.model('profile', profileSchema);
