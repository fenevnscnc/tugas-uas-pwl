var mongoose = require('mongoose');
var crypto = require('crypto');

var secret = 'brotherhood2013';
var password = crypto.createHmac('sha256', secret).update('admin').digest('hex');

console.log('password: ' + password);

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/crud_pegawai');

var User = require('../models/User');

User.find({username:'admin'}, function(err, user){
 if(user.length == 0){
  var admin = new User({
   username: 'superadmin',
   email: 'superadmin@gmail.com',
   password: password,
   name: 'admin',
   admin: true
  });
  admin.save(function(err){
   if(err) throw err;
   console.log('admin dibuat');
  });
 }
});
